PSX-PHP-REACT CRUD Sample
===

## About

This is a sample application which can bootstrap a project based on the PSX 
framework using  REST API as back-end and React UI as front-end

## Install Backend API - PHP

You can install the required libs through composer

> /php-api/composer install

Create a database on a MySql server named: psx-react
Import SQL schema from file : /php-api/schema.txt

To run the back-end part execute:
- Change to folder: 

> cd /php-api/public 

-Run the API:
> php -S 127.0.0.1:8080 server.php

Open the browser with the url:

> http://127.0.0.1:8080/population

## Install Frontend - React

The Front-end part use react in order to interact with the back-end part. Install required libs:

> npm install

to run the front-end part execute :

> npm run serve

for avoiding CORS problem on chrome browser, execute it with the flag:

 --disable-web-security --user-data-dir="D:\chrome

Open the browser with the url:

> http://localhost:3000 

 
 enjoy it :P!