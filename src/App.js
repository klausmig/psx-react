import React, { Component } from "react";
import { HashRouter as Router, Route, Link, Switch } from "react-router-dom";
import { hot } from "react-hot-loader";
import "./App.scss";
import Menu from "./scenes/Menu/Menu";
import Profile from "./scenes/Profile/Profile";

//import logo from "./images/logo_medium.png";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={Menu} />
            // <Route path="/profile" component={Profile} />
            // <Route path="/menu" component={Menu} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default hot(module)(App);