import React from "react";
import { Link } from "react-router-dom";
import "./ProgressBar.scss";
import utils from "../../../../services/api/utils/utils";

class ProgressBar extends React.Component {
  constructor(props) {
    super(props);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
    this.state = {
      isLoggedIn: false
    };

    this.utils = new utils();
  }

  calculateBarTimePorcent() {
    return this.utils.calculateBarTimePorcent(
      this.props.startTime,
      this.props.endTime
    );
  }

  handleLoginClick() {
    this.setState({
      isLoggedIn: true
    });
  }

  handleLogoutClick() {
    this.setState({
      isLoggedIn: false
    });
  }

  render() {
    return (
      <div className="show-progress">
        <div className="progress">
          <div
            className="progress-bar progress-bar-success"
            role="progressbar"
            aria-valuenow="40"
            aria-valuemin="0"
            aria-valuemax="100"
            style={{ width: this.calculateBarTimePorcent() }}
          />
        </div>
      </div>
    );
  }
}

export default ProgressBar;
