import React from "react";
import ProgressBar from "../progressbar/progressbar";
import utils from "../../../../services/api/utils/utils";
import "./ItemDetail.scss";
//import logo from "./assets/logo_small.png";

class ItemDetail extends React.Component {
  constructor(props) {
    super(props);
    this.assestsPath = window.tapsteryAssetsPath;

    this.onChangePlace = this.onChangePlace.bind(this);
    this.onChangeRegion = this.onChangeRegion.bind(this);
    this.onChangePopulation = this.onChangePopulation.bind(this);
    this.onChangeUsers = this.onChangeUsers.bind(this);
    this.onChangeWorldUsers = this.onChangeWorldUsers.bind(this);

    this.utils = new utils();
  }

  addModifyItem() {
    this.props.addModifyItem(this.props.objItem);
  }

  onChangePlace(e) {
    this.props.objItem.place = parseInt(e.target.value);
  }

  onChangeRegion(e) {
    this.props.objItem.region = e.target.value;
  }

  onChangePopulation(e) {
    this.props.objItem.population = parseInt(e.target.value);
  }

  onChangeUsers(e) {
    this.props.objItem.users = parseInt(e.target.value);
  }

  onChangeWorldUsers(e) {
    this.props.objItem.worldUsers = Number(e.target.value);
  }

  render() {
    return (
      <form>
        <div className="form-group row">
          <label htmlFor="place" className="col-4 col-form-label">
            Place
          </label>
          <div className="col-8">
            <input
              className="form-control"
              type="number"
              name="place"
              defaultValue={this.props.objItem.place}
              onChange={this.onChangePlace}
            />
          </div>
        </div>

        <div className="form-group row">
          <label htmlFor="region" className="col-4 col-form-label">
            Region
          </label>
          <div className="col-8">
            <input
              className="form-control"
              type="text"
              name="region"
              onChange={this.onChangeRegion}
              defaultValue={this.props.objItem.region}
            />
          </div>
        </div>

        <div className="form-group row">
          <label htmlFor="population" className="col-4 col-form-label">
            Population
          </label>
          <div className="col-8">
            <input
              className="form-control"
              type="number"
              name="population"
              onChange={this.onChangePopulation}
              defaultValue={this.props.objItem.population}
            />
          </div>
        </div>

        <div className="form-group row">
          <label htmlFor="users" className="col-4 col-form-label">
            Users
          </label>
          <div className="col-8">
            <input
              className="form-control"
              type="number"
              name="users"
              onChange={this.onChangeUsers}
              defaultValue={this.props.objItem.users}
            />
          </div>
        </div>

        <div className="form-group row">
          <label htmlFor="worldUsers" className="col-4 col-form-label">
            worldUsers
          </label>
          <div className="col-8">
            <input
              className="form-control"
              type="number"
              name="worldUsers"
              onChange={this.onChangeWorldUsers}
              defaultValue={this.props.objItem.worldUsers}
            />
          </div>
        </div>

        <div className="form-group row ">
          <div className="col-3">
            <button
              className="btn btn-success"
              type="button"
              onClick={() => {
                this.addModifyItem();
              }}
            >
              {" "}
              {this.props.submitState}{" "}
            </button>
          </div>
          <div className="col-9">
            <button className="btn btn-primary">Cancel</button>
          </div>
        </div>
      </form>
    );
  }
}

export default ItemDetail;
