import React from "react";
import { Link } from "react-router-dom";
import "./MenuTableRow.scss";

import ProgressBar from "../progressbar/progressbar";
import utils from "../../../../services/api/utils/utils";

class MenuTableRow extends React.Component {
  constructor(props) {
    super(props);

    this.isDeleted = false;

    this.utils = new utils();
  }

  setSelectedItem() {
    if (!this.isDeleted) {
      this.props.getSelectedItem(this.props.objPopulation);
      this.props.setSubmitState("Update");
    }
  }

  removeSelectedItem() {
    this.props.removeSelectedItem(this.props.objPopulation);

    this.isDeleted = true;
  }

  render() {
    return (
      <tr
        onClick={() => {
          this.setSelectedItem();
        }}
      >
        <th scope="row">{this.props.objPopulation.id}</th>
        <td>{this.props.objPopulation.place}</td>
        <td>{this.props.objPopulation.region}</td>
        <td>{this.props.objPopulation.population}</td>
        <td>{this.props.objPopulation.users}</td>
        <td>{this.props.objPopulation.worldUsers}%</td>

        <td>
          <button
            type="button"
            className="btn btn-danger btn-sm"
            onClick={() => {
              this.removeSelectedItem();
            }}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

export default MenuTableRow;
