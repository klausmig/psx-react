import axios from "axios";
import React from "react";
import MenuTableRow from "./components/MenuTableRow/MenuTableRow";
import ItemDetail from "./components/ItemDetail/ItemDetail";
import apiPopulation from "../../services/api/population/population";

import "./Menu.scss";

class Menu extends React.Component {
  constructor(props) {
    super(props);

    this.getSelectedItem = this.getSelectedItem.bind(this);
    this.removeSelectedItem = this.removeSelectedItem.bind(this);
    this.createNewItem = this.createNewItem.bind(this);
    this.addModifyItem = this.addModifyItem.bind(this);
    this.setSubmitState = this.setSubmitState.bind(this);

    this.postDataChange = this.postDataChange.bind(this);
    this.putDataChange = this.putDataChange.bind(this);

    this.state = {
      selectedItem: null,
      submitState: "Update",
      data: null
    };
  }

  getLivePopulation() {
    var livePopulation = new apiPopulation();

    var that = this;

    axios
      .get("http://127.0.0.1:8080/population")
      .then(res => {
        console.log(res.data.entry);

        that.setState({
          data: res.data.entry,
          selectedItem: res.data.entry[0]
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  componentDidMount() {
    this.getLivePopulation();
  }

  componentWillUnmount() {}

  getSelectedItem(obj) {
    this.setState({
      selectedItem: obj
    });
  }

  setSubmitState(value) {
    this.setState({
      submitState: value
    });
  }

  createNewItem() {
    const maxId =
      Math.max.apply(
        Math,
        this.state.data.map(function(o) {
          return o.id;
        })
      ) + 1;

    var newObj = {
      id: maxId,
      place: maxId,
      region: "test",
      population: 0,
      users: 0,
      worldUsers: 0,
      datetime: "2009-11-29T15:21:49Z"
    };

    this.setState({
      selectedItem: newObj,
      submitState: "Create"
    });
  }

  removeSelectedItem(obj) {
    var array = [...this.state.data]; // make a separate copy of the array
    var index = array.indexOf(obj);
    if (index !== -1) {
      array.splice(index, 1);
      this.setState({
        data: array,
        selectedItem: array[0]
      });

      axios
        .delete("http://127.0.0.1:8080/population/" + obj.id)
        .then(res => {
          console.log(res.data);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

  postDataChange(array, obj) {
    axios
      .post("http://127.0.0.1:8080/population/", obj)
      .then(res => {
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });

    this.setState({
      data: array,
      selectedItem: obj,
      submitState: "Update"
    });
  }

  putDataChange(array, obj) {
    axios
      .put("http://127.0.0.1:8080/population/" + obj.id, obj)
      .then(res => {
        console.log("sucess:", res.data);
      })
      .catch(err => {
        console.log(err);
      });

    this.setState({
      data: array,
      selectedItem: obj
    });
  }

  addModifyItem(obj) {
    if (this.state.submitState == "Create") {
      var array = [...this.state.data, obj];

      this.postDataChange(array, obj);
    } else {
      var array = [...this.state.data];
      this.putDataChange(array, obj);
    }
  }

  render() {
    var that = this;

    if (!this.state.data) {
      return <div />;
    }
    var arrayentry = this.state.data;

    var entry = arrayentry.map(function(obj) {
      return (
        <MenuTableRow
          key={obj.id}
          objPopulation={obj}
          setSubmitState={that.setSubmitState}
          removeSelectedItem={that.removeSelectedItem}
          getSelectedItem={that.getSelectedItem}
        />
      );
    });

    return (
      <div>
        <div className="content container">
          <div className="card">
            <div className="card-header">
              <h5>List of Populations </h5>
            </div>
            <div className="card-body">
              <table className="table table-hover table-sm">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Place</th>
                    <th scope="col">Region</th>
                    <th scope="col">Population</th>
                    <th scope="col">users</th>
                    <th scope="col">worldUsers</th>
                    <th scope="col" colSpan="2" />
                  </tr>
                </thead>
                <tbody>{entry}</tbody>
              </table>

              <div className="text-left">
                <button
                  type="text"
                  className="btn btn-primary"
                  onClick={() => {
                    this.createNewItem();
                  }}
                >
                  New
                </button>
              </div>
            </div>
          </div>

          <div className="current-item col-6">
            <div className="card">
              <div className="card-header">
                <h5>{this.state.submitState} Population</h5>
              </div>
              <div className="card-body">
                <ItemDetail
                  key={this.state.selectedItem.id}
                  objItem={this.state.selectedItem}
                  addModifyItem={this.addModifyItem}
                  submitState={this.state.submitState}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Menu;
