class Utils {
	constructor() {}

	formatDuration(dateTime) {
		var array = dateTime.split(":");
		var hour = array[0];
		var minuten = array[1];

		var resultString = "";

		console.log(hour);

		if (hour)
			if (!hour !== "00") {
				if (minuten !== "00") {
					return array[0] + ":" + array[1];
				}
			} else {
				if (!array[1].equals("00")) {
					return array[1];
				}
			}
	}

	formatDateTime(dateTime) {
		//console.log("dateTime", dateTime);
		var now = new Date(dateTime);
		return (
			now.getHours() +
			":" +
			(now.getMinutes() < 10 ? "0" + now.getMinutes() : now.getMinutes())
		);
	}

	diff_hours(dt2, dt1) {
		var diff = (dt2.getTime() - dt1.getTime()) / 1000;
		diff /= 60 * 60;
		return Math.abs(Math.round(diff));
	}

	daysBetween(date1, date2) {
		//Get 1 day in milliseconds
		var one_day = 1000 * 60;

		// Convert both dates to milliseconds
		var date1_ms = date1.getTime();
		var date2_ms = date2.getTime();

		// Calculate the difference in milliseconds
		var difference_ms = date2_ms - date1_ms;

		// Convert back to days and return
		return Math.round(difference_ms / one_day);
	}

	calculateRestTime(endTime) {
		//console.log("dateTime", dateTime);
		var endDateTime = new Date(endTime);
		var now = this.daysBetween(new Date(), endDateTime);
		console.log(now);

		return now;
		// return (

		// 	((now.getMinutes() < 10) ?
		// 		("0" + now.getMinutes()) :
		// 		(now.getMinutes())));
	}

	calculateBarTimePorcent(startTime, endTime) {
		//console.log("dateTime", dateTime);
		var startTimeShow = new Date(startTime);
		var endTimeShow = new Date(endTime);
		var now = new Date();

		var porcent =
			((now - startTimeShow) * 100) / (endTimeShow - startTimeShow);

		porcent = porcent.toFixed(2);

		//console.log("porcent", porcent);

		return porcent + "%";
	}
}
export default Utils;
