CREATE TABLE IF NOT EXISTS `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `address` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `authorId` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `author` (`id`, `name`, `address`) VALUES
(1, 'admin', 'COLOMBIA');

INSERT INTO `book` (`id`, `authorId`, `title`, `description`) VALUES
(1, 1, 'Metamorfis', 'Bokk about the change ..'),
(2, 1, 'Harry Potter', 'life of ..');


CREATE TABLE IF NOT EXISTS `population` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place` int(11) NOT NULL,
  `region` varchar(32) NOT NULL,
   `population` int(11) NOT NULL,
    `users` int(11) NOT NULL,
    `worldUsers` DECIMAL(5,1) NOT NULL,
    
  `datetime` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `population` (`id`, `place`, `region`,  `population`, `users`, `worldUsers`,`datetime`) VALUES
(1, 1, 'China', 1338612968,360000000,20.8,"2009-11-29T15:21:49+00:00"),
(2, 2, 'United States', 307212123,227719000,13.1,"2009-11-29T15:22:40+00:00"),
(3, 3, 'Japan', 127078679,95979000,5.5,"2009-11-29T15:23:18Z"),
(4, 4, 'India', 1156897766,81000000,4.7,"2009-11-29T15:24:47Z");
